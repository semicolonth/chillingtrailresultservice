require('dotenv').config();

const express = require('express');
const axios = require('axios');

const app = express();

const listenIP = process.env.listenIP;
const listenPort = process.env.listenPort;


app.disable('x-powered-by');

app.get('/result/:year?/:race?/:id?', (req, res, next) => {
    //res.send('Welcome to Chilling Trail Result Service');
    // console.log('Year: ' + req.params.year);
    // console.log('Race: ' + req.params.race);
    // console.log('ID: ' + req.params.id);
    axios.get('http://utmb.livetrail.net/coureur.php?rech=' + req.params.id)
      .then((result) => {
        //console.log(result.data);
        var obj = {};
        obj.identity = {};
        var match = /<identite([^\/]*)\/>/m.exec(result.data);
        if(match !== null) {
          console.log('identite matched');
          var m = /nom="([^"]*)"/m.exec(match[1]);
          if(m !== null) {
            obj.identity.nom = m[1];            
          }
          var m = /prenom="([^"]*)"/m.exec(match[1]);
          if(m !== null) {
            obj.identity.prenom = m[1];            
          }
          var m = /club="([^"]*)"/m.exec(match[1]);
          if(m !== null) {
            obj.identity.club = m[1];            
          }
        }

        obj.state = {};
        var match = /<state([^\/]*)\/>/m.exec(result.data);
        if(match !== null) {
          console.log('state matched');
          var m = /\sclt="([^"]*)"/m.exec(match[1]);
          if(m !== null) {
            obj.state.overallRank = m[1];            
          }
          var m = /\scltcat="([^"]*)"/m.exec(match[1]);
          if(m !== null) {
            obj.state.categoryRank = m[1];            
          }
          var m = /\scode="([^"]*)"/m.exec(match[1]);
          if(m !== null) {
            obj.state.code = m[1];            
          }
        }

        var match = /<fiche([^>]*)>/m.exec(result.data);
        if(match !== null) {
          console.log('fiche matched');
          var m = /\sdoss="([^"]*)"/m.exec(match[1]);
          if(m !== null) {
            obj.identity.bib = m[1];            
          }
          var m = /\sc="([^"]*)"/m.exec(match[1]);
          if(m !== null) {
            obj.identity.race = m[1];            
          }
        }

        obj.points = []
        var match = /<pts>([\s\S]*)<\/pts>/m.exec(result.data);
        if(match !== null) {
          console.log('pts matched');
          var m = match[1].match(/<pt([^/>]*)\/>/gm);
          if(m !== null) {
            m.forEach((ptText) => {
              var pt = {};
              var mat = /idpt="([^"]*)"/.exec(ptText);
              if(mat !== null) {
                pt.idpt = mat[1];
              }
              var mat = /\sn="([^"]*)"/.exec(ptText);
              if(mat !== null) {
                pt.n = mat[1];
              }
              var mat = /\snc="([^"]*)"/.exec(ptText);
              if(mat !== null) {
                pt.nc = mat[1];
              }
              var mat = /\skm="([^"]*)"/.exec(ptText);
              if(mat !== null) {
                pt.km = mat[1];
              }
              obj.points.push(pt)
            })
          }
        }

        var match = /<pass>([\s\S]*)<\/pass>/m.exec(result.data);
        if(match !== null) {
          console.log('pass matched');
          var m = match[1].match(/<e([^>]*)>/gm);
          if(m !== null) {
            m.forEach((ptText) => {
              var idpt = 0;
              var mat = /idpt="([^"]*)"/.exec(ptText);
              if(mat !== null) {
                idpt = mat[1];
              }
              for(i = 0; i < obj.points.length; i++) {
                if(obj.points[i].idpt == idpt) {
                  break;
                }
              }
              if(i < obj.points.length) {
                var mat = /hd="([^"]*)"/.exec(ptText);
                if(mat !== null) {
                  obj.points[i].hd = mat[1];
                }
                var mat = /ha="([^"]*)"/.exec(ptText);
                if(mat !== null) {
                  obj.points[i].ha = mat[1];
                }
                var mat = /tps="([^"]*)"/.exec(ptText);
                if(mat !== null) {
                  obj.points[i].tps = mat[1];
                }
                var mat = /clt="([^"]*)"/.exec(ptText);
                if(mat !== null) {
                  obj.points[i].overallRank = mat[1];
                }
  
              }
            })
          }
        }
        console.log(obj);
        res.status(200).json(obj);
      })
      .catch((err) => {
        console.log(err)
        res.send('ERROR')
      })
});

app.listen(listenPort, listenIP, function () {
  console.log('Chilling Trail Result Service Listening on ' + listenIP + ':' + listenPort);
});
